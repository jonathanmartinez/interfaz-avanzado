//
//  Producto.h
//  Wedding
//
//  Created by Máster Móviles on 10/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Producto : NSObject

@property(nonatomic,strong) NSString* name;
@property(nonatomic) float total;
@property(nonatomic) float funded;

- initWithName:(NSString*)name andTotal:(float)total andFunded:(float)funded;

@end
