//
//  main.m
//  WeddingJonathanMartinez
//
//  Created by Máster Móviles on 11/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
