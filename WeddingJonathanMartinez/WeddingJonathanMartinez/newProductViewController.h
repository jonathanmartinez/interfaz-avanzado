//
//  newProductViewController.h
//  Wedding
//
//  Created by Máster Móviles on 11/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Producto.h"

@protocol newProductProtocol <NSObject>
-(void) didCreateNewProduct: (Producto*) producto;
@end

@interface newProductViewController : UIViewController
@property (nonatomic,strong) id<newProductProtocol> delegate;

@property (weak, nonatomic) IBOutlet UITextField *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *totalLabel;


@end
