//
//  newProductViewController.m
//  Wedding
//
//  Created by Máster Móviles on 11/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import "newProductViewController.h"

@interface newProductViewController ()

@end

@implementation newProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)saveProduct:(id)sender {
    Producto *p = [[Producto alloc] initWithName:self.nameLabel.text
                                        andTotal:[self.totalLabel.text floatValue]
                                       andFunded:0.0];
    if([self.delegate respondsToSelector:@selector(didCreateNewProduct:)]){
        [self.delegate didCreateNewProduct:p];
            [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
