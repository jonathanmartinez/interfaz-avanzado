//
//  DetailViewController.h
//  WeddingJonathanMartinez
//
//  Created by Máster Móviles on 11/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Producto.h"
#import "DLPieChart.h"

@interface DetailViewController : UIViewController

@property (weak, nonatomic) Producto* detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *funded;
@property (weak, nonatomic) IBOutlet UILabel *total;
@property (nonatomic, retain) IBOutlet DLPieChart *pieChartView;

@end

