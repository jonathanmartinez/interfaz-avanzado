//
//  EditProductViewController.h
//  WeddingJonathanMartinez
//
//  Created by Máster Móviles on 11/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Producto.h"

@interface EditProductViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *fundedField;

@property (weak, nonatomic) Producto *producto;

@end
