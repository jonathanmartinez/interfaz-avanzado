//
//  Producto.m
//  Wedding
//
//  Created by Máster Móviles on 10/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import "Producto.h"

@implementation Producto

- (id)init {
    self = [super init];
    if(self)
    {
        _name = @"Sin nombre";
        _total = 0.0;
        _funded = 0.0;
    }
    return self;
}

- (id)initWithName: (NSString*)name andTotal:(float)total andFunded:(float)funded {
    self = [super init];
    if(self){
        _name = name;
        _total = total;
        _funded = funded;
    }
    return self;
}
@end
