//
//  DetailViewController.m
//  WeddingJonathanMartinez
//
//  Created by Máster Móviles on 11/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import "DetailViewController.h"
#import "EditProductViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController



#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        self.detailDescriptionLabel.text = self.detailItem.name;
        self.funded.text = [NSString stringWithFormat:@"%.2f €", self.detailItem.funded];
        self.total.text = [NSString stringWithFormat:@"%.2f €", self.detailItem.total];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    
    [_detailItem addObserver:self
             forKeyPath:@"funded"
                options:0
                context: nil];
    
    NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:2];
    
    [dataArray addObject:@(_detailItem.funded)];
    [dataArray addObject:@(_detailItem.total)];
    
    [self.pieChartView renderInLayer:self.pieChartView dataArray:dataArray];
}

//KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
 {
     self.funded.text = [NSString stringWithFormat:@"%.2f €",_detailItem.funded];
     NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:2];
     
     [dataArray addObject:@(_detailItem.funded)];
     [dataArray addObject:@(_detailItem.total)];
     
     [self.pieChartView renderInLayer:self.pieChartView dataArray:dataArray];
 }

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"goToEditProduct"]) {
        EditProductViewController *VC = (EditProductViewController*)segue. destinationViewController;
        VC.producto = _detailItem;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
