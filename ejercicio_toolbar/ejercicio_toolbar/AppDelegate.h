//
//  AppDelegate.h
//  ejercicio_toolbar
//
//  Created by Máster Móviles on 04/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

