//
//  ViewController.m
//  ejercicio_toolbar
//
//  Created by Máster Móviles on 04/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Creamos la imagen de fondo para el toolbar
    UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondo_madera.png"]];
    iv.frame = CGRectMake(0, 0, self.toolBar.frame.size.width, self.toolBar.frame.size.height);
    iv.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // Añadimos la subview
    [self.toolBar insertSubview:iv atIndex:1];
    
    // Cambiamos el color del Segmented Control
    [self.segmentedControl setTintColor:[UIColor brownColor]];
    
    // Personalizamos el text field:
    self.textField.textColor = [UIColor whiteColor]; //texto de color blanco
    self.textField.borderStyle = UITextBorderStyleNone; //quitamos el borde del campo
    self.textField.background = [UIImage imageNamed:@"fondo_textfield.png"]; //fondo
    [self.textField setPlaceholder:@"Escribe aquí"]; //texto inicial
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) segmentedControlIndexChanged{
    switch (self.segmentedControl.selectedSegmentIndex) {
        case 0:
            self.segmentLabel.text = @"Segmento 1 seleccionado.";
            break;
        case 1:
            self.segmentLabel.text = @"Segmento 2 seleccionado.";
            break;
        default:
            break;
    }
}

@end
