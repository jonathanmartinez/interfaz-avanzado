//
//  TableViewCell.m
//  ejercicio_celdas
//
//  Created by Máster Móviles on 04/11/14.
//  Copyright (c) 2014 Máster Móviles. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
